/*
 * Copyright (c) Charmaine Pascual Calsas
 * cpcalsas011@gmail.com
 */

package exercise;

import domain.Item;

/**
 *
 * @author Maine
 */ 
public class shoppingCart {
     public static void main(String[] args) {
         /*
         String custName = "Charmaine Calsas";
         String itemDesc = "Shampoo";
         double price = 500.0;
         double tax = 0.05;
         int quantity = 5;
         double total = price * quantity * tax;
             
         String[] items = {"Shirt","Blouse","Caps","Shoes"};
         System.out.println(items.length);
         System.out.println(items[4]);
         
                 
         String message = custName + " wants to purchase " +quantity + " " +itemDesc;
         boolean outOfStock = false;       
         
        if(quantity>1){
             System.out.println(message + "s");
         }  
         
         if(outOfStock){
             System.out.println("Item is unavailable");
         }else{
             System.out.println(message);
             System.out.println("Total cost with tax is: " +total);
         }
         */
         
         Item item01 = new Item(001,"Shirt",100,250.00);
         Item item02 = new Item(002,"Shoes",200,350.00);
         
         item01.displayItem();
         item02.displayItem();
         
         item02.displayMessage("Test");
         
         item01.setColor('R');
                
    }
}
