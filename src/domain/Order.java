/*
 * Copyright (c) Charmaine Pascual Calsas
 * cpcalsas011@gmail.com
 */

package domain;

/**
 *
 * @author Maine
 */
public class Order {
    private int ID;
    private Customer customer;
    private double totalPrice;
    private Item[] items = new Item[5];
    private int counter = 0;

    public Order(int ID, Customer customer) {
        this.ID = ID;
        this.customer = customer;
    }

    public int getID() {
        return ID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public Item[] getItem() {
        return items;
    }

    public Item addItem(Item item){
        items[counter] = item;
        counter++;
        return item;
    }
    
    public Item removeItem(Item item){
        for (int x = 0; x < items.length; x++) {
            if(items[x].getID() == item.getID()){
                items[x] = null;
            }
        }
        return item;
    }
    
    public void calculateTotal(){
        for (int i = 0; i < items.length; i++) {
            if(items[i]!=null){
                totalPrice += items[i].getPrice() *items[i].getQuantity();
            }
        }
    }
    
}
