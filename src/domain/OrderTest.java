/*
 * Copyright (c) Charmaine Pascual Calsas
 * cpcalsas011@gmail.com
 */

package domain;

/**
 *
 * @author Maine
 */
public class OrderTest {
    public static void main(String[] args) {
        Customer cust01 = new Customer(01, "Charmaine Calsas", "cpcalsas011@gmail.com", "6501011");
        
        Item item01 = new Item(0001, "Nike Shoes", 2, 5000.00);
        Item item02 = new Item(0002, "Shirt", 1, 300.00);
        Item item03 = new Item(0003, "Cap", 1, 450.00);
        Item item04 = new Item(0004, "Necklace", 1, 1500.00);
        Item item05 = new Item(0005, "Watch", 1, 4000.00);
        
        Order order01 = new Order(000001,cust01);
        
        order01.addItem(item01);
        order01.addItem(item02);
        order01.addItem(item03);
        order01.addItem(item04);
        order01.addItem(item05);
        
        order01.removeItem(item04);
        
        order01.calculateTotal();
        
        System.out.println("Customer Name: "+cust01.getName());
        System.out.println("Order 01: "+order01.getTotalPrice());
        
    }
}
