/*
 * Copyright (c) Charmaine Pascual Calsas
 * cpcalsas011@gmail.com
 */

package domain;

/**
 *
 * @author Maine
 */
public class Customer {
    private int ID;
    private String name;
    private String email;
    private String phone;

    public Customer(int ID, String name, String email, String phone) {
        this.ID = ID;
        this.name = name;
        this.email = email;
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void displayCustomer(){
        System.out.println("ID: " +ID);
        System.out.println("Name: "+name);
        System.out.println("Email: "+email);
        System.out.println("Phone: "+phone);
    }
    
}
