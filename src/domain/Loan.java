/*
 * Copyright (c) Charmaine Pascual Calsas
 * cpcalsas011@gmail.com
 */

package domain;

import java.util.Date;

/**
 *
 * @author Maine
 */
public class Loan {
    public Date dueDate;
    public Book book;

//    public Loan(Date dueDate, Book book) {
//        this.dueDate = dueDate;
//        this.book = book;
//    }

    public Loan(Book book) {
        this.book = book;
        dueDate = calculateDueDate();
    }
    
    public Date calculateDueDate(){
        //current date + book.noOfDays
        return new Date();
    }
    
}
