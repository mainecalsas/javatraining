/*
 * Copyright (c) Charmaine Pascual Calsas
 * cpcalsas011@gmail.com
 */

package domain;

/**
 *
 * @author Maine
 */
public class Book {
    private int itemNumber;
    private String status;
    private String title;
    private String author;
    private int noOfDays = 7;

    
    //constructor
    public Book(int itemNumber, String status, String title, String author) {
        this.itemNumber = itemNumber;
        this.status = status;
        this.title = title;
        this.author = author;
    }
    
    //getter
    public int getItemNumber() {
        return itemNumber;
    }

    public String getStatus() {
        return status;
    }

    //setter
    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(int noOfDays) {
        this.noOfDays = noOfDays;
    }

    //method
    public void displayBook(){
        System.out.println("itemNumber: "+itemNumber);
        System.out.println("Title: "+title);
        System.out.println("Author: "+author);
        System.out.println("status: "+status);
    }


}

