/*
 * Copyright (c) Charmaine Pascual Calsas
 * cpcalsas011@gmail.com
 */

package domain;

/**
 *
 * @author Maine
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        
        
        Book book1 = new Book(001, "Available", "Twilight Series", "Stephanie Meyer"); //create new Book
        
        Loan loan = new Loan(book1);
        
        //Date dueDate = new Date();
        //Loan loan2 = new Loan(dueDate, book1);
        
        Book book2 = new Book(002, "Unavailable", "Harry Potter", "JK Rowling"); //create new Book

        book1.setAuthor("Stephen "); //use setter setAuthor
        
        System.out.println(book1.getAuthor()); //use getter getAuthor
        
        book1.displayBook();
        
        book2.displayBook();

    }
    
}
