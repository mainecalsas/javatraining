/*
 * Copyright (c) Charmaine Pascual Calsas
 * cpcalsas011@gmail.com
 */

package domain;

/**
 *
 * @author Maine
 */
public class Item {
    private int ID;
    private String descr;
    private int quantity;
    private double price;
    private char color;
    
    public Item(int ID, String descr, int quantity, double price) {
        this.ID = ID;
        this.descr = descr;
        this.quantity = quantity;
        this.price = price;
    }

    public int getID() {
        return ID;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public char getColor() {
        return color;
    }
    
    public void displayMessage(String message){
        System.out.println(message);
    }
    
    public void displayItem(){
        System.out.println("ID: " +getID());
        System.out.println("Description: " +getDescr());
        System.out.println("Quantity: "+getQuantity());
        System.out.println("Price: "+getPrice());
    }
    
    public boolean setColor(char color){
        if((color == 'R')||(color == 'G')||(color == 'B')){
            System.out.println("Valid");
            return true;
        }else{
            System.out.println("Not Valid");
            return false;
        }

    }
    
}
